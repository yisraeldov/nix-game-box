{ lib, stdenv, buildPackages, binutils ? null, util-linux, fetchFromGitHub
, libX11, imlib2, hicolor-icon-theme }:
stdenv.mkDerivation rec {
  pname = "xlunch";
  version = "4.7.1";

  src = fetchFromGitHub {
    owner = "Tomas-M";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-iPrpzMldZrwLAZT/a7NnV1th2x5BuPL0uQOOUEXz6vM=";
    fetchSubmodules = true;
  };

  preBuild = ''
     substituteInPlace "extra/genentries" \
        --replace '$(eval echo ~''${SUDO_USER})' "/tmp/"
  '';
  buildInputs = [ libX11 imlib2 hicolor-icon-theme];
  makeFlags = ["xlunch"];
  installPhase = ''
    install -D  -v -t $out/bin/  xlunch
    install -D  -v -t $out/etc/xlunch/  default.conf
  '';

  meta = with lib; {
    homepage = "http://xlunch.org/";
    description = "xlunch";
    license = licenses.gpl3;
    platforms = platforms.linux;
    maintainers = with maintainers; [ ];
  };
}
