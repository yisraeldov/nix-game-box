{ config, pkgs, lib, ... }:

let
  user = "guest";
  password = "guest";
  secrets = import ./secrets.nix;
  SSID = secrets.SSID;
  SSIDpassword = secrets.SSIDpassword;
  interface = "wlan0";
  hostname = "rpi-game";
  xlunch = pkgs.callPackage ./xlunch {};
in {
  # load the vendor kerne
  imports = [
    "${
      fetchTarball
      "https://github.com/NixOS/nixos-hardware/archive/081907627cf036c4259cd22260c041ac462ea0da.tar.gz"

    }/raspberry-pi/4"
  ];

  nix.autoOptimiseStore = true;
  nixpkgs.config.allowUnfree = true;

  boot = {
    kernelParams = [
      # to get ShanWan Twin USB Joystick working
      # https://forum.batocera.org/d/4326-24g-wireless-shanwan-twin-usb-joystick-issue
      #"usbhid.quirks=0×2563:0×555:0×040,0×2563:0×555:0×005"
    ];
    extraModprobeConfig = ''
      options snd_bcm2835 enable_headphones=1
    '';

    loader.raspberryPi.firmwareConfig = ''
      dtparam=audio=on
        '';
  };
  sound = {
    enable = true;
    mediaKeys.enable = true;
  };

  hardware = {
    # Enable GPU acceleration
    raspberry-pi."4".fkms-3d.enable = true;
    raspberry-pi."4".audio.enable = true;
    pulseaudio.enable = true;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };

  networking = {
    hostName = hostname;
    wireless = {
      enable = true;
      #if you don't have your networks in a secrets file
      #networks."${SSID}".psk = SSIDpassword;

      networks = secrets.networking.wireless.networks;
      interfaces = [ interface ];
    };
  };

  swapDevices = [{
    device = "/swapfile";
    size = 3072;
  }];

  environment.systemPackages = with pkgs; [
    xlunch
    nano
    emacs
    emulationstation
    git
    retroarch
    #    attract-mode
    mari0
    superTuxKart
    superTux
    # (openttd.overrideAttrs(oldAttrs: {
    #   buildInputs = [icu ] ++ oldAttrs.buildInputs;
    # }))
    openttd
    neverball
    tuxpaint
    (retroarch.override {
      cores = with libretro; [
        snes9x
        dosbox
        # fbneo
        # gambatte
        # mgba
        # #mrboom
        # mupen64plus
        fceumm
        # beetle-pce-fast
        # genesis-plus-gx
        # flycast
        # picodrive
        # duckstation
        # ppsspp
        # beetle-vb
      ];
    })
  ];

  services.openssh.enable = true;

  users = {
    mutableUsers = false;
    users."${user}" = {
      isNormalUser = true;
      password = password;
      extraGroups = [ "wheel" ];
    };
  };

  services.xserver = {
    enable = true;
    displayManager.lightdm.enable = true;
    desktopManager.xfce.enable = true;
    displayManager.defaultSession = "xlunch";
    displayManager.autoLogin.user = "guest";
    displayManager.autoLogin.enable = true;
    displayManager.session = [
      {
        manage = "desktop";
        name = "xlunch";
        start = ''
          ${xlunch}/bin/xlunch -f /nix/store/y74mhag6zkx7c3rpyr5xx4h98p1kv9hf-dejavu-fonts-2.37/share/fonts/truetype/DejaVuSans.ttf/12  &
            waitPID=$!
        '';
      }
      {
        manage = "desktop";
        name = "emulationstation";
        start = ''
          pactl set-default-sink alsa_output.platform-bcm2835_audio.stereo-fallback.2
          pactl set-sink-volume alsa_output.platform-bcm2835_audio.stereo-fallback.2 .99
          ${pkgs.emulationstation}/bin/emulationstation  &
          waitPID=$!
        '';
      }
      {
        manage = "desktop";
        name = "attract-mode";
        start = ''
          ${pkgs.attract-mode}/bin/attract-mode  &
          waitPID=$!
        '';
      }

      {
        manage = "desktop";
        name = "retroarch";
        start = ''
          ${pkgs.retroarch}/bin/retroarch  &
          waitPID=$!
        '';
      }
    ];
  };

  environment.etc = {
    "emulationstation/es_systems.cfg" = {
      text = ''
        <systemList>
          <!-- Here's an example system to get you started. -->
          <system>

            <name>native</name>

            <path>~/launchers/</path>

            <extension>.sh</extension>
            <command>bash  %ROM%</command>
            <theme>ports</theme>
          </system>
        </systemList>
      '';
    };
    "emulationstation/gamelists/native/gamelist.xml" = {
      text = ''
        <gameList>
          <game>
            <path>${pkgs.openttd}/bin/openttd</path>
            <name>OpenTtd</name>
            <desc>${pkgs.openttd.meta.description}</desc>
            <image>${pkgs.openttd}/share/icons/hicolor/256x256/apps/openttd.png</image>
          </game>
        </gameList>
              '';
    };
  };
  services.avahi = {
    enable = true;
    publish.enable = true;
    publish.workstation = true;
    #   extraServiceFiles.ssh = [ "${pkgs.avahi}/etc/avahi/services/ssh.service" ];
  };

  #  services.zerotierone = {
  #     enable = true;
  #     joinNetworks = secrets.zerotierone.networks
  #  };

}
